import React,{useState,useEffect} from 'react';
// import Cors from "cors";
import nc from "next-connect";
import cors from "cors";
import { Config } from '../config';
import axios from 'axios';
import NextCors from 'nextjs-cors';



const AddJob = () => {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [nonce, setNonce] = useState("");

    useEffect(() => {
        // fetchData()
     
    }, [])

    const formSubmit = (e) => {
        e.preventDefault();


        const postData = {title,description}

        // const headers = {
        //     'Content-Type': 'application/json;charset=UTF-8',
        //     'X-WP-Nonce' : '865108ebd6',
        //     "status" : "publish"
        // }

        // running cors middleware to circumwent localhost CORS issue
            async function handler(req, res) {
                // Run the cors middleware
                // nextjs-cors uses the cors package, so we invite you to check the documentation https://github.com/expressjs/cors
                await NextCors(req, res, {
                // Options
                methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
                origin: '*',
                optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
                });
            
                // Rest of the API logic

                fetch(`${Config.apiUrl}`)
                        .then(response => console.log(response.json()))
                        // .then(re);
                        .then(response => console.log(response))

                  setTitle('');
                  setDescription('');
                res.json({ message: 'Hello NextJs Cors!' });
            }

       

       

        // fetch('http://jby.local/wp-json/wp/v2/posts', {
        //     method: 'POST',
        //     credentials: 'same-origin',
        //     headers: new Headers({
        //         'Content-Type': 'application/json;charset=UTF-8',
        //         'X-WP-Nonce' : '865108ebd6',
        //         "status" : "publish"
        //     }),
        //     body: JSON.stringify(postData),
        //   }).then(response => {
        //     console.log(response);
        //     return response.json();
        //   });



        
    }


    return(
        <div>
                <h3>Add Job</h3>
                <form action="" onSubmit={formSubmit}>
                    <p>Job Title</p>
                    <input type="text" name="title" placeholder="Job Title" value={title} onChange={(e) => setTitle(e.target.value)}/>

                    <p>Job Description</p>
                    <input type="text" name="description" placeholder="Job Description" value={description} onChange={(e) => setDescription(e.target.value)} />
                    <br />
                    <button type="submit">Submit</button>
                </form>
            
        </div>
    )
}


export default AddJob;