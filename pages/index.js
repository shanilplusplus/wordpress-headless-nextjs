import {ApolloClient, InMemoryCache, gql} from '@apollo/client';
import { Config } from '../config';
import axios from 'axios';



export default function Home({data}) {


  
  //testing getting post data from wordpress
  const getData = async () => {

    try{
      const data = axios.get(`${process.env.BACKEND_URL}`)
      data.then((res) => {
        console.log(res, "res")
      })
      return data
    } catch(error){
      console.log(error)
    }

  }

  

  return (
   <div>
      <h3>Yolo</h3>

      <div className="posts">
          {data && data.map((e) => {
            return(
              <div key={e.id}>
                {/* displaying all the titles */}
              <p>{e.node.slug}</p>
              </div>
            )
          })}
      </div>

   </div>
  )
}


export async function getStaticProps(context) {

  const client = new ApolloClient({
    uri: process.env.BACKEND_URL,
    cache: new InMemoryCache()
  })

  const {data} = await client.query({
    query: gql`
    query MyQuery {
      posts {
        edges {
          node {
            id
            slug
            title
            content
          }
        }
      }
    }
    `
  })

  //getting post data serverside
  return {
    props: {
      data: data.posts.edges

    }, // will be passed to the page component as props
  }
}


