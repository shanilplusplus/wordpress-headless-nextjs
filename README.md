
## Built With

* [React.js](https://reactjs.org/) - for React and ReactDom libraries
* [Wordpress](https://github.com/expressjs/express) - Headless Backend
* [Graphql](https://github.com/porsager/postgres) - for querying 
* [Apollo](https://github.com/sequelize/sequelize) - Client
* [Next.js](https://www.npmjs.com/package/Babel) - Front End



## Authors

* **Shaun Sigera** (http://sigera.ca/)


## License

This project is licensed under the MIT License


